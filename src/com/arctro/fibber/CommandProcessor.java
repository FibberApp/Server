package com.arctro.fibber;

import java.util.HashMap;

import com.arctro.fibber.game.Game;
import com.arctro.fibber.game.commands.BuildGameCommand;
import com.arctro.fibber.game.commands.Command;
import com.arctro.fibber.game.commands.CreateGameBuilderCommand;
import com.arctro.fibber.game.commands.JoinGameCommand;
import com.arctro.fibber.game.commands.PingCommand;
import com.arctro.fibber.game.commands.ServerTimeCommand;
import com.arctro.fibber.game.controllers.GameController;
import com.arctro.fibber.models.CommandRequest;
import com.arctro.fibber.models.CommandResponse;
import com.arctro.fibber.supporting.exceptions.ClientException;
import com.arctro.fibber.supporting.exceptions.InvalidCommandException;

/**
 * Recieves commands from the client and either processes them or passes them onto another
 * processor. Acts similarly to a command line
 * @author Ben McLean
 *
 */
public class CommandProcessor{
	
	static HashMap<String, Command> globalCommands = new HashMap<String, Command>();
	static{
		globalCommands.put(Command.CREATE_GAME, new CreateGameBuilderCommand());
		globalCommands.put(Command.JOIN_GAME, new JoinGameCommand());
		globalCommands.put(Command.BUILD_GAME, new BuildGameCommand());
		globalCommands.put(Command.PING, new PingCommand());
		globalCommands.put(Command.SERVER_TIME, new ServerTimeCommand());
	}

	/**
	 * Takes in a CommandRequest from the client, and produces a response
	 * @param request The request to process
	 * @return A response
	 */
	public static CommandResponse process(CommandRequest request){
		CommandResponse cr = null;
		
		//Get the global command
		Command command = globalCommands.get(request.getCommand());
		if(command == null){
			//Get the players associated game
			Game game = GameController.getGameByPlayer(request.getSessionId());
			
			//Check if the player is in a game
			if(game == null){
				return CommandResponse.exception(request.getRequestToken(), new ClientException("Client has no game"));
			}
			
			//Get the local command
			command = game.getCommand(request.getCommand());
			
			//Check if the command exists
			if(command == null){
				return CommandResponse.exception(request.getRequestToken(), new InvalidCommandException("Command does not exist"));
			}
		}
		
		cr = command.execute(request);
		
		//If no response was generated make blank
		if(cr == null){
			cr = CommandResponse.blank();
		}
		
		//Set the request token
		cr.setRequestToken(request.getRequestToken());
		return cr;
	}
}
