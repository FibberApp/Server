package com.arctro.fibber;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.arctro.fibber.supporting.Consts;
import com.arctro.myplace.connectionresource.ConnectionFactory;
import com.arctro.myplace.connectionresource.pools.DatasourceConnectionPool;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

/**
 * Runs on server startup
 * @author Ben McLean
 *
 */
@WebListener
public class Initialize implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0){
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0){
		//Load the MySQL driver
		try{
			Class.forName("com.mysql.jdbc.Driver");
		}catch(ClassNotFoundException e){
			e.printStackTrace();
		}
		
		//Register the quiz database connection
		registerConn(Consts.QUIZ_CONNECTION, "jdbc:mysql://localhost:3306/fibber","root","");
	}
	
	/**
	 * Registers a database connection with the connection factory
	 * @param name The connection name
	 * @param url The url to the database
	 * @param username The username of the user
	 * @param password The password of the user
	 */
	private static void registerConn(String name, String url, String username, String password){
		HikariConfig config = new HikariConfig();
		config.setJdbcUrl(url);
		config.setUsername(username);
		config.setPassword(password);
		
		//Register with the connection pool
		ConnectionFactory.addConnectionPool(name, new DatasourceConnectionPool(new HikariDataSource(config)));
	}

}
