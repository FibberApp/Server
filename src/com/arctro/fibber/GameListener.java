package com.arctro.fibber;

import java.io.IOException;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.json.JSONObject;

import com.arctro.fibber.controllers.ResourceManager;
import com.arctro.fibber.game.Game;
import com.arctro.fibber.game.commands.Command;
import com.arctro.fibber.game.controllers.GameBuilder;
import com.arctro.fibber.game.controllers.GameBuilderController;
import com.arctro.fibber.game.controllers.GameController;
import com.arctro.fibber.models.CommandRequest;
import com.arctro.fibber.models.CommandResponse;
import com.arctro.fibber.supporting.Consts;
import com.arctro.fibber.websocket.WebsocketWrapper;

/**
 * Listens for requests, joins, and leaves from the client
 * @author Ben McLean
 *
 */
@ServerEndpoint("/game")
public class GameListener{

	@OnOpen
	public void onOpen(Session session){
		System.out.println("Connection open");
		
		WebsocketWrapper ww = new WebsocketWrapper(session);
		
		//Lease and set the session id
		session.getUserProperties().put(Consts.SESSION_ID_PARAM, ResourceManager.lease(ww));
		
		//Send server time
		CommandRequest cr = new CommandRequest(Command.SERVER_TIME, session, "", new JSONObject());
		CommandResponse cs = CommandProcessor.process(cr);
		try{
			ww.transmit(new JSONObject(cs).toString());
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	@OnClose
	public void onClose(Session session){
		System.out.println("Connection closed");
		
		//Get the user's session id
		String sid = (String) session.getUserProperties().get(Consts.SESSION_ID_PARAM);
		//Release the session id for reuse and broadcast
		ResourceManager.release(sid);
		
		//Removes player from any lobbies they're in
		GameBuilder gb = GameBuilderController.getByPlayer(sid);
		if(gb != null){
			gb.leave(sid);
		}
		
		//Removes player from any games they're in
		Game g = GameController.getGameByPlayer(sid);
		if(g != null){
			g.leave(sid);
		}
	}
	
	@OnMessage
	public void onMessage(String message, Session session){
		//Process the message
		JSONObject jMessage = new JSONObject(message);
		//Build a command
		CommandRequest cr = new CommandRequest(jMessage.getString("command"), session, jMessage.getString("requestToken"), jMessage.getJSONObject("payload"));
		//Process and return the result of the command
		
		CommandResponse cs = CommandProcessor.process(cr);
		
		WebsocketWrapper ww = WebsocketWrapper.getWebsocketWrapper(session);
		
		if(cs != null){
			try{
				ww.transmit(new JSONObject(cs).toString());
			}catch(IOException e){
				e.printStackTrace();
			}
		}
	}
	
	@OnError
	public void onError(Throwable e){
		e.printStackTrace();
	} 
}
