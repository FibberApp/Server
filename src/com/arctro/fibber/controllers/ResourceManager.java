package com.arctro.fibber.controllers;

import java.util.HashMap;

/**
 * Manages all resources and their respective IDs. Resources include:
 * Players, Games and Game Controllers
 * @author Ben McLean
 *
 */
public class ResourceManager{
	
	//Holds all resources
	static HashMap<String, Object> resources = new HashMap<String, Object>();
	static int lastID = 1;
	
	/**
	 * Lease a new resource ID, and register it
	 * @param s The object to register
	 * @return The resource ID
	 */
	public static String lease(Object s){
		String token;
		token = (++lastID)+"r";
		
		//Register
		resources.put(token, s);
		
		//Return
		return token;
	}
	
	/**
	 * Releases a resource ID and resource
	 * @param token The resource to release
	 */
	public static void release(String token){
		resources.remove(token);
	}
	
	/**
	 * Returns a resource by it's ID
	 * @param token The resource ID
	 * @return The resource
	 */
	public static Object getById(String token){
		return resources.get(token);
	}
	
}
