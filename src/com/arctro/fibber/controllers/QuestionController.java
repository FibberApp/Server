package com.arctro.fibber.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.arctro.fibber.models.Category;
import com.arctro.fibber.models.Question;
import com.arctro.fibber.supporting.Consts;
import com.arctro.myplace.connectionresource.ConnectionFactory;

/**
 * Retrieves and controls Questions
 * @author Ben McLean
 *
 */
public class QuestionController{
	
	/**
	 * Selects a random question from a category
	 * @param c The category to select from
	 * @return A random question
	 * @throws SQLException SQL Exception
	 */
	public static Question getRandomQuestionFromCategory(Category c) throws SQLException{
		Connection conn = null;
		
		try{
			//Open a connection
			conn = ConnectionFactory.getConnection(Consts.QUIZ_CONNECTION);
			//Get a random question
			PreparedStatement prepared = conn.prepareStatement("SELECT `id`, `question` FROM `questions` WHERE `category_id` = ? ORDER BY RAND() LIMIT 1");
			prepared.setInt(1, c.getCategoryId());
			
			ResultSet rs = prepared.executeQuery();
			
			//If none was found
			if(!rs.next()){
				return null;
			}
			
			return new Question(rs.getInt("id"), c, rs.getString("question"));
		}finally{
			//Close the connection
			if(conn != null){
				conn.close();
			}
		}
	}
	
}
