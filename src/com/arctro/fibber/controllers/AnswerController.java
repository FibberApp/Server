package com.arctro.fibber.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.arctro.fibber.models.Answer;
import com.arctro.fibber.models.Question;
import com.arctro.fibber.supporting.Consts;
import com.arctro.myplace.connectionresource.ConnectionFactory;

/**
 * Retrieves and controlls answers
 * @author Ben McLean
 *
 */
public class AnswerController{
	
	/**
	 * Retrieves a list of random answers 
	 * @param q The question to get answers for
	 * @param size The number of answers to get
	 * @return A list populated with random answers
	 * @throws SQLException SQL Exception
	 */
	public static List<Answer> getRandomAnswers(Question q, int size) throws SQLException{
		Connection conn = null;
		
		try{
			//Open a connection
			conn = ConnectionFactory.getConnection(Consts.QUIZ_CONNECTION);
			
			//Query for a list of randomly ordered answers of size
			PreparedStatement prepared = conn.prepareStatement("SELECT `answer` FROM `answers` WHERE `question_id` = ? AND `correct` = 0 ORDER BY RAND() LIMIT ?");
			prepared.setInt(1, q.getQuestionId());
			prepared.setInt(2, size);
			
			//Execute query
			ResultSet rs = prepared.executeQuery();
			List<Answer> answers = new ArrayList<Answer>();
			
			//Fill list
			while(rs.next()){
				Answer answer = new Answer(null, null, rs.getString("answer"), true, false);
				answer.setAnswerId(ResourceManager.lease(answer));
				
				answers.add(answer);
			}
			
			return answers;
		}finally{
			//Close the connection
			if(conn != null){
				conn.close();
			}
		}
	}
	
	/**
	 * Gets the answer to a question
	 * @param q The question to get the answer to
	 * @return The answer to a question
	 * @throws SQLException SQL Exception
	 */
	public static Answer getCorrectAnswer(Question q) throws SQLException{
		Connection conn = null;
		
		try{
			//Open a connection
			conn = ConnectionFactory.getConnection(Consts.QUIZ_CONNECTION);
			
			//Query for the correct answer
			PreparedStatement prepared = conn.prepareStatement("SELECT `answer` FROM `answers` WHERE `question_id` = ? AND `correct` = 1");
			prepared.setInt(1, q.getQuestionId());
			
			//Return null if there is no correct answer
			ResultSet rs = prepared.executeQuery();
			if(!rs.next()){
				return null;
			}
			
			Answer answer = new Answer(null, null, rs.getString("answer"), true, false);
			answer.setAnswerId(ResourceManager.lease(answer));
			
			return answer;
		}finally{
			//Close the connection
			if(conn != null){
				conn.close();
			}
		}
	}
	
}
