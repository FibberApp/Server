package com.arctro.fibber.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.arctro.fibber.models.Category;
import com.arctro.fibber.supporting.Consts;
import com.arctro.myplace.connectionresource.ConnectionFactory;

/**
 * Retrieves and controls Categories
 * @author Ben McLean
 *
 */
public class CategoryController{
	
	/**
	 * Gets the default list of categories available to every player
	 * @return The default list of categories available to every player
	 * @throws SQLException SQL Exception
	 */
	public static List<Category> getDefaultCategories() throws SQLException{
		Connection conn = null;
		
		try{
			//Get a connection
			conn = ConnectionFactory.getConnection(Consts.QUIZ_CONNECTION);
			//Execute SQL statement
			PreparedStatement prepared = conn.prepareStatement("SELECT * FROM `categories`");
			
			ResultSet rs = prepared.executeQuery();
			List<Category> categories = new ArrayList<Category>();
			
			//Add all categories
			while(rs.next()){
				categories.add(new Category(rs.getInt("id"), rs.getString("name")));
			}
			
			return categories;
		}finally{
			//Close the connection
			if(conn != null){
				conn.close();
			}
		}
	}
	
}
