package com.arctro.fibber.supporting;

import org.json.JSONObject;

public class ErrorPayload{
	
	public static final JSONObject GAME_BUILDER_DOES_NOT_EXIST = e("GBDNE", "Requested GameBuilder does not exist");
	
	public static JSONObject e(String code, String reason){
		JSONObject j = new JSONObject();
		j.put("code", code);
		j.put("reason", reason);
		
		return j;
	}
	
}
