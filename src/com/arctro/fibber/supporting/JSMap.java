package com.arctro.fibber.supporting;

import java.util.Collection;

import com.arctro.fibber.models.Player;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;

public class JSMap{
	
	public static Collection<String> playerToSession(Collection<Player> player){
		return Collections2.transform(player, new PlayerToSessionFunction());
	}
	
	public static class PlayerToSessionFunction implements Function<Player, String>{

		@Override
		public String apply(Player input){
			return input.getSessionId();
		}
		
	}
	
}
