package com.arctro.fibber.supporting;

import java.io.IOException;
import java.security.SecureRandom;

import javax.websocket.RemoteEndpoint.Basic;
import javax.websocket.Session;

public class Utils{
	
	public static SecureRandom random = new SecureRandom();
	
	public static int randomOfLength(int len){
		int max = (int)Math.pow(10, len)-1;
		int min = (int)Math.pow(10, len-1);
		
	    return random(max, min);
	}
	
	public static byte[] randomBytes(int len){
		byte[] b = new byte[len];
		random.nextBytes(b);
		return b;
	}
	
	public static String randomString(int len){
		return new String(randomBytes(len));
	}
	
	public static int random(int max, int min){
		if(max == min){
			return min;
		}
		
		return random.nextInt(max - min + 1) + min;
	}
	
	public static void sleep(long time){
		try{
			Thread.sleep(time);
		}catch(InterruptedException e){
		}
	}
	
	public synchronized static void send(Session session, String message) throws IOException{
		Basic basic = session.getBasicRemote();
		basic.sendText(message);
	}
}
