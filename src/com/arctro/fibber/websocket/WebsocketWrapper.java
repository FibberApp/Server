package com.arctro.fibber.websocket;

import java.io.IOException;

import javax.websocket.Session;

import com.arctro.fibber.controllers.ResourceManager;
import com.arctro.fibber.supporting.Consts;

public class WebsocketWrapper{
	public static WebsocketWrapper getWebsocketWrapper(Session session){
		return (WebsocketWrapper) ResourceManager.getById((String) session.getUserProperties().get(Consts.SESSION_ID_PARAM));
	}
	
	Session session;
	
	public WebsocketWrapper(Session session){
		super();
		this.session = session;
	}

	public Session getSession(){
		return session;
	}

	public void setSession(Session session){
		this.session = session;
	}
	
	
	public synchronized void transmitRetry(String message, int attempts){
		for(int i = 0; i < attempts; i++){
			try{
				transmit(message);
				return;
			}catch(IOException e){
				e.printStackTrace();
			}
		}
	}
	
	public synchronized void transmit(String message) throws IOException{
		session.getBasicRemote().sendText(message);
	}
}
