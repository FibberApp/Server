package com.arctro.fibber.game;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.arctro.fibber.broadcasts.CategorySelectBroadcast;
import com.arctro.fibber.broadcasts.InterruptBroadcast;
import com.arctro.fibber.broadcasts.PlayerLeaveBroadcast;
import com.arctro.fibber.broadcasts.PlayerSelectedAnswerBroadcast;
import com.arctro.fibber.broadcasts.QuestionBroadcast;
import com.arctro.fibber.broadcasts.SelectAnswerBroadcast;
import com.arctro.fibber.controllers.AnswerController;
import com.arctro.fibber.controllers.QuestionController;
import com.arctro.fibber.controllers.ResourceManager;
import com.arctro.fibber.game.commands.Command;
import com.arctro.fibber.game.controllers.GameController;
import com.arctro.fibber.models.Answer;
import com.arctro.fibber.models.Category;
import com.arctro.fibber.models.Player;
import com.arctro.fibber.models.PlayerScoreData;
import com.arctro.fibber.models.PlayerSelectedAnswer;
import com.arctro.fibber.models.Question;
import com.arctro.fibber.models.TimeLimit;
import com.arctro.fibber.supporting.Utils;

/**
 * The game itself
 * @author Ben McLean
 *
 */
public class Game{
	/**
	 * The default game configuration
	 */
	public static final GameConfig DEFAULT_CONFIG = new GameConfig();
	
	String gameId;
	boolean running = false;
	GameConfig config = DEFAULT_CONFIG;
	HashMap<String, Command> commands;
	private Thread t;
	
	GameState state = GameState.NO_INTERACT;
	
	HashMap<String, Player> players;
	List<Category> categoryPool;
	
	//Globals for interaction between Commands and GL functions.
	//These are necessary because of the asynchronous nature of Websockets
	Category selectedCategory = null;
	Player categorySelectingPlayer;
	List<Answer> playerAnswers = null;
	List<Player> playerWaitList = null;
	HashMap<String, List<Player>> playerAnswerSelection = null; //<AnswerID, Player>
	int numSelected = 0;
	int numAcknowledged = 0;
	
	public Game(){
		commands = new HashMap<String, Command>();
		players = new HashMap<String, Player>();
		categoryPool = new ArrayList<Category>();
		playerAnswerSelection = new HashMap<String, List<Player>>();
	}
	
	/**
	 * The main game loop. Only one can be run per object
	 */
	public void start(){
		//Ensure this is the only running instance
		if(running){
			return;
		}
		running = true;
		
		//Run in a new thread
		t = new Thread(){
			public void run(){
				//Run for the configured rounds
				for(int round = 0; round < config.getRounds() && running; round++){
					//Reset everything
					GLreset();
					
					//Do select category
					state = GameState.CATEGORY_SELECT;
					Category category = GLselectCategory();
					
					//Manage category pool
					if(categoryPool.size() > config.getMinCategoryPoolSize()){
						categoryPool.remove(category);
					}
					
					//Check if game has ended
					if(!running){
						return;
					}
					
					//Do submit answer
					state = GameState.SUBMIT_ANSWER;
					List<Answer> answers = GLsubmitAnswers(category);
					
					//Check if game has ended
					if(!running){
						return;
					}
					
					//Do select answer
					state = GameState.SELECT_ANSWER;
					GLselectAnswers(answers);
					
					//Check if game has ended
					if(!running){
						return;
					}
					
					state = GameState.ACKNOWLEDGE;
					setNumAcknowledged(0);
					
					Utils.sleep(600000);
				}
				
				//End the game
				running = false;
			}
		};
		t.start();
	}
	
	/**
	 * Stops the main game loop
	 */
	public void stop(){
		running = false;
	}
	
	/**
	 * Resets the game loop state
	 */
	private void GLreset(){
		selectedCategory = null;
		categorySelectingPlayer = null;
		
		//Release player answers
		if(playerAnswers != null){
			for(Answer answer: playerAnswers){
				ResourceManager.release(answer.getAnswerId());
			}
		}
		
		setNumSelected(0);
		
		playerAnswers = new ArrayList<Answer>();
		playerAnswerSelection = new HashMap<String, List<Player>>();
	}
	
	/**
	 * Prompts a user to select a category. If no category is entered within the time limit
	 * a random category is returned.
	 * @return A category
	 */
	private Category GLselectCategory(){
		//Get the player that will select the category
		
		Iterator<Player> pI = players.values().iterator();
		while(pI.hasNext()){
			Player p = pI.next();
			
			if(p.getName().equals("a")){
				categorySelectingPlayer = p;
			}
		}
		
		if(categorySelectingPlayer == null){
			categorySelectingPlayer = getRandomPlayer();
		}
		
		//Get the list of categories the player can select from
		List<Category> pool = getCategoryPool(config.getPlayerSelectCategoryPoolMax());
		
		//Broadcast to players
		new CategorySelectBroadcast()
			.setCategories(pool)
			.setSelector(categorySelectingPlayer)
			.setTimeLimit(new TimeLimit(config.getCategorySelectTime()))
			.addRecipients(players.values())
			.broadcast();
		
		//Wait for the time limit
		Utils.sleep(config.getCategorySelectTime());
		
		//If no category was selected
		if(selectedCategory == null){
			//Get random category from pool
			selectedCategory = pool.get(Utils.random(pool.size() - 1, 0));
		}
		
		return selectedCategory;
	}
	
	/**
	 * Prompts users to submit answers to the question. If a user forfeits or does
	 * not answer within the time limit an answer is chosen randomly by the 
	 * computer
	 * @param q The question to answer
	 * @return A list of the answers
	 */
	private List<Answer> GLsubmitAnswers(Category category){
		//Get a random question
		Question q = null;
		try{
			q = QuestionController.getRandomQuestionFromCategory(category);
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		new QuestionBroadcast()
			.setQuestion(q)
			.setTimeLimit(new TimeLimit(config.getEnterAnswerTime()))
			.addRecipients(players.values())
			.broadcast();
		
		//Wait for the time limit
		Utils.sleep(config.getEnterAnswerTime());	
		
		//Get a list of players without answers
		List<Player> playersWithoutAnswers = new ArrayList<Player>(players.values());
		for(int i = 0; i < playerAnswers.size(); i++){
			Answer a = playerAnswers.get(i);
			//If the player forfeits remove from valid answer so it can be replaced
			if(a.isForfit()){
				ResourceManager.release(a.getAnswerId());
				playerAnswers.remove(a);
				continue;
			}
			
			//By default remove player from playersWithoutAnswers, since if they appear in playerAnswers they have answered
			playersWithoutAnswers.remove(a.getAnswerer());
		}
		
		//Get a list of random answers 
		List<Answer> cAnswers = new ArrayList<Answer>();
		Answer correct = null;
		try{
			cAnswers = AnswerController.getRandomAnswers(q, playersWithoutAnswers.size());
			correct = AnswerController.getCorrectAnswer(q);
		}catch(SQLException e){
			e.printStackTrace();
		}
		for(int i = 0; i < cAnswers.size(); i++){
			//Add player
			Answer a = cAnswers.get(i);
			a.setAnswerer(playersWithoutAnswers.get(i));
		}
		
		//Add computer answers to list of real answers
		playerAnswers.addAll(cAnswers);
		playerAnswers.add(correct);
		return playerAnswers;
	}
	
	/**
	 * Waits for players to select their answers
	 * @param answers The answers to select
	 */
	private void GLselectAnswers(List<Answer> answers){
		new SelectAnswerBroadcast()
			.setAnswers(answers)
			.setTimeLimit(new TimeLimit(config.getSelectAnswerTime()))
			.addRecipients(players.values())
			.broadcast();
		
		//Wait for the time limit
		Utils.sleep(config.getSelectAnswerTime());
		
		//Prepare the player scores
		HashMap<String, PlayerScoreData> playerScores = new HashMap<String, PlayerScoreData>(); //Session ID, PLayer Score Data
		for(Player player: players.values()){
			playerScores.put(player.getSessionId(), new PlayerScoreData(player, 0));
		}
		List<PlayerSelectedAnswer> playerSelectedAnswers = new ArrayList<PlayerSelectedAnswer>();
		
		//For every answer tally the scores to each respective player
		for(Answer answer: answers){
			//Get the players that selected the answer
			List<Player> selectedAnswer = playerAnswerSelection.get(answer.getAnswerId());
			
			//If anyone selected the answer
			if(selectedAnswer != null){
				playerSelectedAnswers.add(new PlayerSelectedAnswer(answer, selectedAnswer));
				
				//If the answer was incorrect
				if(answer.getAnswerer() != null){
					//For every player that selected the answer
					for(Player player: selectedAnswer){
						//Calculate the score to be added, (i.e. if the answer was written by the player or computer)
						int added = (answer.isComputerAnswer()) ? config.getYourComputerAnswerPickedScore() : config.getYourAnswerPickedScore();
						//Get the player that submitted the answer's score data for tallying
						PlayerScoreData answerer = playerScores.get(answer.getAnswerer().getSessionId());
						//Add the score to the player's absolute score
						answerer.getPlayer().setScore(answerer.getPlayer().getScore() + added);
						//Add the score to the player's round score
						answerer.setScored(answerer.getScored() + added);
					}
				
				//If the answer was correct
				}else{
					//For every player that selected the answer
					for(Player player: selectedAnswer){
						//Get the player's score data for tallying
						PlayerScoreData answerer = playerScores.get(player.getSessionId());
						//Add the score to the player's absolute score
						answerer.getPlayer().setScore(answerer.getPlayer().getScore() + config.getCorrectAnswerPickedScore());
						//Add the score to the player's round score
						answerer.setScored(answerer.getScored() + config.getCorrectAnswerPickedScore());
					}
				}
			}
		}
		
		new PlayerSelectedAnswerBroadcast()
			.setPlayerScoreData(playerScores.values())
			.setPlayerSelectedAnswers(playerSelectedAnswers)
			.addRecipients(players.values())
			.broadcast();
	}
	
	/**
	 * Wait until all users send an acknowledgement command, or a timeout. Once
	 * the timeout is hit all unacknowledged users are returned in a list so
	 * they can be removed
	 * @param timeout The time to wait before stopping waiting
	 * @return A list of unacknowledged players
	 */
	private List<Player> GLwaitUntilAllAcknowledged(long timeout){
		long start = System.currentTimeMillis();
		
		//Sleep until players acknowledged count reaches player count or timeout reached
		while(playerWaitList.size() < players.size() && (System.currentTimeMillis() - start) < timeout){
			Utils.sleep(100);
		}
		
		//Make a list of all unacknowledged players
		if(playerWaitList.size() < players.size()){
			List<Player> unacknowledgedPlayers = new ArrayList<Player>();
			unacknowledgedPlayers.removeAll(playerWaitList);
			return unacknowledgedPlayers;
		}
		
		return new ArrayList<Player>();
	}
	
	/**
	 * Returns a random player
	 * @return A random player
	 */
	public Player getRandomPlayer(){
		//Turn the player HashMap into a List
		List<Player> pl = new ArrayList<Player>(players.values());
		//Return a random player at index 0 to Max
		return pl.get(Utils.random(pl.size()-1, 0));
	}
	
	/**
	 * Returns a subpool of the category pool of size
	 * @param size The size to make the subpool
	 * @return A subpool of categories of size or less
	 */
	public List<Category> getCategoryPool(int size){
		//If there arn't enough categories left to fully populate the subpool return the remaining items
		if(size >= categoryPool.size()){
			return categoryPool;
		}
		
		//Create a list of every index in the list
		List<Integer> indexList = new ArrayList<Integer>();
		for(int i = 0; i < categoryPool.size(); i++){
			indexList.add(i);
		}
		//Shuffle the list, this creates a list of numbers which is random and unique
		Collections.shuffle(indexList);
		
		//Create a subpool from the shuffled index list
		List<Category> subpool = new ArrayList<Category>();
		for(int i = 0; i < size; i++){
			subpool.add(categoryPool.get(indexList.get(i)));
		}
		
		return subpool;
	}
	
	public void interrupt(){
		t.interrupt();
		
		new InterruptBroadcast()
			.addRecipients(players.values())
			.broadcast();
	}
	
	public void leave(List<Player> players){
		for(Player player: players){
			leave(player.getSessionId());
		}
	}
	
	public void leave(String sid){
		
		new PlayerLeaveBroadcast()
			.setLeft(players.remove(sid))
			.addRecipients(getPlayers().values())
			.broadcast();
		
		if(players.size() < 1){
			running = false;
			GameController.deregister(sid);
		}
	}
	
	public Command getCommand(String name){
		return commands.get(name);
	}
	
	public Player getPlayer(String sessionId){
		return players.get(sessionId);
	}

	public boolean isRunning(){
		return running;
	}

	public void setRunning(boolean running){
		this.running = running;
	}

	public GameConfig getConfig(){
		return config;
	}

	public void setConfig(GameConfig config){
		this.config = config;
	}

	public HashMap<String, Command> getCommands(){
		return commands;
	}

	public void setCommands(HashMap<String, Command> commands){
		this.commands = commands;
	}

	public GameState getState(){
		return state;
	}

	public void setState(GameState state){
		this.state = state;
	}

	public HashMap<String, Player> getPlayers(){
		return players;
	}

	public void setPlayers(HashMap<String, Player> players){
		this.players = players;
	}

	public List<Category> getCategoryPool(){
		return categoryPool;
	}

	public void setCategoryPool(List<Category> categoryPool){
		this.categoryPool = categoryPool;
	}

	public Category getSelectedCategory(){
		return selectedCategory;
	}

	public void setSelectedCategory(Category selectedCategory){
		this.selectedCategory = selectedCategory;
	}

	public Player getCategorySelectingPlayer(){
		return categorySelectingPlayer;
	}

	public void setCategorySelectingPlayer(Player categorySelectingPlayer){
		this.categorySelectingPlayer = categorySelectingPlayer;
	}

	public List<Answer> getPlayerAnswers(){
		return playerAnswers;
	}

	public void setPlayerAnswers(List<Answer> playerAnswers){
		this.playerAnswers = playerAnswers;
	}

	public List<Player> getPlayerWaitList(){
		return playerWaitList;
	}

	public void setPlayerWaitList(List<Player> playerWaitList){
		this.playerWaitList = playerWaitList;
	}

	public String getGameId(){
		return gameId;
	}

	public void setGameId(String gameId){
		this.gameId = gameId;
	}

	public HashMap<String, List<Player>> getPlayerAnswerSelection(){
		return playerAnswerSelection;
	}

	public void setPlayerAnswerSelection(HashMap<String, List<Player>> playerAnswerSelection){
		this.playerAnswerSelection = playerAnswerSelection;
	}

	public int getNumSelected(){
		return numSelected;
	}

	public void setNumSelected(int numSelected){
		this.numSelected = numSelected;
	}

	public int getNumAcknowledged(){
		return numAcknowledged;
	}

	public void setNumAcknowledged(int numAcknowledged){
		this.numAcknowledged = numAcknowledged;
	}
	
	
}
