package com.arctro.fibber.game;

/**
 * Stores the configuration of a Game
 * @author Ben McLean
 */
public class GameConfig{
	//How many rounds the game will go for
	int rounds = 5;
	
	//How long the player should have to select a category
	int categorySelectTime = 30*1000;
	//How long to show the question for
	int showQuestionTime = 20*1000;
	//How long the players have to enter an answer
	int enterAnswerTime = 60*1000;
	//How long plaers have to select an answer
	int selectAnswerTime = 60*1000;
	
	//Score for when someone picks your answer
	int yourAnswerPickedScore = 250;
	//Score for when someone picks your answer that is from the computer
	int yourComputerAnswerPickedScore = 100;
	//Score for when you pick a correct answer
	int correctAnswerPickedScore = 500;
	//Score for when you question is voted funniest by a user
	int funniestAnswerVoteScore = 100;
	
	//The default maximum size of the selectable category pool
	int playerSelectCategoryPoolMax = 5;
	//The minimum size of the category pool
	int minCategoryPoolSize = 2;
	
	public GameConfig(){}

	public GameConfig(int rounds, int categorySelectTime, int showQuestionTime, int enterAnswerTime,
			int selectAnswerTime, int yourAnswerPickedScore, int yourComputerAnswerPickedScore,
			int correctAnswerPickedScore, int funniestAnswerVoteScore, int playerSelectCategoryPoolMax,
			int minCategoryPoolSize){
		super();
		this.rounds = rounds;
		this.categorySelectTime = categorySelectTime;
		this.showQuestionTime = showQuestionTime;
		this.enterAnswerTime = enterAnswerTime;
		this.selectAnswerTime = selectAnswerTime;
		this.yourAnswerPickedScore = yourAnswerPickedScore;
		this.yourComputerAnswerPickedScore = yourComputerAnswerPickedScore;
		this.correctAnswerPickedScore = correctAnswerPickedScore;
		this.funniestAnswerVoteScore = funniestAnswerVoteScore;
		this.playerSelectCategoryPoolMax = playerSelectCategoryPoolMax;
		this.minCategoryPoolSize = minCategoryPoolSize;
	}

	public int getYourComputerAnswerPickedScore(){
		return yourComputerAnswerPickedScore;
	}

	public void setYourComputerAnswerPickedScore(int yourComputerAnswerPickedScore){
		this.yourComputerAnswerPickedScore = yourComputerAnswerPickedScore;
	}

	public int getMinCategoryPoolSize(){
		return minCategoryPoolSize;
	}

	public void setMinCategoryPoolSize(int minCategoryPoolSize){
		this.minCategoryPoolSize = minCategoryPoolSize;
	}

	public int getPlayerSelectCategoryPoolMax(){
		return playerSelectCategoryPoolMax;
	}

	public void setPlayerSelectCategoryPoolMax(int playerSelectCategoryPoolMax){
		this.playerSelectCategoryPoolMax = playerSelectCategoryPoolMax;
	}

	public int getRounds(){
		return rounds;
	}

	public void setRounds(int rounds){
		this.rounds = rounds;
	}

	public int getCategorySelectTime(){
		return categorySelectTime;
	}

	public void setCategorySelectTime(int categorySelectTime){
		this.categorySelectTime = categorySelectTime;
	}

	public int getShowQuestionTime(){
		return showQuestionTime;
	}

	public void setShowQuestionTime(int showQuestionTime){
		this.showQuestionTime = showQuestionTime;
	}

	public int getEnterAnswerTime(){
		return enterAnswerTime;
	}

	public void setEnterAnswerTime(int enterAnswerTime){
		this.enterAnswerTime = enterAnswerTime;
	}

	public int getSelectAnswerTime(){
		return selectAnswerTime;
	}

	public void setSelectAnswerTime(int selectAnswerTime){
		this.selectAnswerTime = selectAnswerTime;
	}

	public int getYourAnswerPickedScore(){
		return yourAnswerPickedScore;
	}

	public void setYourAnswerPickedScore(int yourAnswerPickedScore){
		this.yourAnswerPickedScore = yourAnswerPickedScore;
	}

	public int getCorrectAnswerPickedScore(){
		return correctAnswerPickedScore;
	}

	public void setCorrectAnswerPickedScore(int correctAnswerPickedScore){
		this.correctAnswerPickedScore = correctAnswerPickedScore;
	}

	public int getFunniestAnswerVoteScore(){
		return funniestAnswerVoteScore;
	}

	public void setFunniestAnswerVoteScore(int funniestAnswerVoteScore){
		this.funniestAnswerVoteScore = funniestAnswerVoteScore;
	}
}
