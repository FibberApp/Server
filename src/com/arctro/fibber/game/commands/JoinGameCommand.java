package com.arctro.fibber.game.commands;

import org.json.JSONArray;
import org.json.JSONObject;

import com.arctro.fibber.game.controllers.GameBuilder;
import com.arctro.fibber.game.controllers.GameBuilderController;
import com.arctro.fibber.models.CommandRequest;
import com.arctro.fibber.models.CommandResponse;
import com.arctro.fibber.models.Player;
import com.arctro.fibber.supporting.Consts;
import com.arctro.fibber.supporting.ErrorPayload;

/**
 * Joins a gameBuilder
 * @author Ben McLean
 */
public class JoinGameCommand implements Command{

	public JoinGameCommand(){}

	@Override
	public String getName(){
		return Command.JOIN_GAME;
	}

	@Override
	public CommandResponse execute(CommandRequest cr){
		//Retrieve the data from the command request
		String gameCode = cr.getPayload().getString("gameCode");
		String name = cr.getPayload().getString("name");
		JSONObject payload = new JSONObject();
		
		//Get the gameBuilder
		GameBuilder gb = GameBuilderController.get(gameCode); 
		//Check if null
		if(gb == null){
			return new CommandResponse(Consts.ERROR_PARAM, cr.getRequestToken(), ErrorPayload.GAME_BUILDER_DOES_NOT_EXIST);
		}
		
		payload.put("currentPlayers", new JSONArray(gb.getCurrentPlayers()));
		
		//Join and generate payload
		Player player = gb.join(cr.getSession(), name);
		payload.put("player", new JSONObject(player));
		
		return new CommandResponse("", payload);
	}

}
