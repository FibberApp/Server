package com.arctro.fibber.game.commands;

import java.util.List;

import org.json.JSONException;

import com.arctro.fibber.game.Game;
import com.arctro.fibber.game.GameState;
import com.arctro.fibber.models.Category;
import com.arctro.fibber.models.CommandRequest;
import com.arctro.fibber.models.CommandResponse;
import com.arctro.fibber.supporting.exceptions.ClientException;
import com.arctro.fibber.supporting.exceptions.InvalidGameStateException;

/**
 * Receives and processes the command to select a category
 * @author Ben McLean
 */
public class CategorySelectCommand implements Command{

	Game game;

	public CategorySelectCommand(Game game){
		this.game = game;
	}

	@Override
	public String getName(){
		return Command.SELECT_CATEGORY;
	}

	@Override
	public CommandResponse execute(CommandRequest cr){
		try{
			execute(cr.getSessionId(), cr.getPayload().getInt("categoryId"));
		}catch(JSONException | InvalidGameStateException | ClientException e){
			e.printStackTrace();
			return CommandResponse.exception(e);
		}
		return null;
	}

	public void execute(String sessionId, int categoryId) throws InvalidGameStateException, ClientException{
		//Check if the game is in the correct state
		if(game.getState() != GameState.CATEGORY_SELECT){
			throw new InvalidGameStateException("The game state must be in SELECT_CATEGORY");
		}
		//Check if the player is allowed to select a category
		if(!game.getCategorySelectingPlayer().getSessionId().equals(sessionId)){
			throw new ClientException("Player cannot select category");
		}

		//Find the category
		List<Category> cp = game.getCategoryPool();
		Category category = null;
		
		for(Category c: cp){
			if(c.getCategoryId() == categoryId){
				category = c;
			}
		}
		
		//If the category is invalid throw an exception
		if(category == null){
			throw new ClientException("Category does not exist");
		}

		//Set the selected category and end the wait
		game.setSelectedCategory(category);
		game.interrupt();
	}

}
