package com.arctro.fibber.game.commands;

import com.arctro.fibber.models.CommandRequest;
import com.arctro.fibber.models.CommandResponse;

public interface Command{
	
	public static final String JOIN_GAME = "join_game";
	public static final String CREATE_GAME = "create_game";
	public static final String CONFIG_GAME = "configure_game";
	public static final String BUILD_GAME = "build_game";
	public static final String SELECT_ANSWER = "select_answer";
	public static final String SELECT_CATEGORY = "select_category";
	public static final String INPUT_ANSWER = "input_answer";
	public static final String PING = "ping";
	public static final String SERVER_TIME = "server_time";
	public static final String ACKNOWLEDGE = "acknowledge";
	
	public String getName();
	
	public CommandResponse execute(CommandRequest cr);
	
}
