package com.arctro.fibber.game.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;

import com.arctro.fibber.game.Game;
import com.arctro.fibber.game.GameState;
import com.arctro.fibber.models.CommandRequest;
import com.arctro.fibber.models.CommandResponse;
import com.arctro.fibber.models.Player;
import com.arctro.fibber.supporting.exceptions.InvalidGameStateException;

public class SelectAnswerCommand implements Command{

	Game game;

	public SelectAnswerCommand(Game game){
		this.game = game;
	}

	@Override
	public String getName(){
		return Command.SELECT_ANSWER;
	}

	@Override
	public CommandResponse execute(CommandRequest cr){
		try{
			execute(cr.getSessionId(), cr.getPayload().getString("answerId"));
		}catch(JSONException | InvalidGameStateException e){
			e.printStackTrace();
			return CommandResponse.exception(e);
		}
		
		return null;
	}

	public void execute(String sessionId, String selectedAnswerId) throws InvalidGameStateException{
		//Check if the game is in the correct state
		if(game.getState() != GameState.SELECT_ANSWER){
			throw new InvalidGameStateException("The game state must be in SELECT_ANSWER");
		}
		
		//Get the list of selected answers
		HashMap<String, List<Player>> pas = game.getPlayerAnswerSelection();
		List<Player> pasl = pas.get(selectedAnswerId);
		
		//Check if the list of players for the chosen answer is null
		if(pasl == null){
			pasl = new ArrayList<Player>();
			pas.put(selectedAnswerId, pasl);
		}
		
		//Add the player to the list
		pasl.add(game.getPlayer(sessionId));
		
		//Count the number of selected players up
		game.setNumSelected(game.getNumSelected()+1);
		
		//Check if all players have selected answers, if they have interrupt
		if(game.getNumSelected() == game.getPlayers().size()){
			game.interrupt();
		}
	}

}