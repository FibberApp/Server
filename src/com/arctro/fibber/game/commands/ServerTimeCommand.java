package com.arctro.fibber.game.commands;

import org.json.JSONObject;

import com.arctro.fibber.models.CommandRequest;
import com.arctro.fibber.models.CommandResponse;

/**
 * Returns the server time for clock syncing
 * @author Ben McLean
 */
public class ServerTimeCommand implements Command{

	public ServerTimeCommand(){}

	@Override
	public String getName(){
		return Command.SERVER_TIME;
	}

	@Override
	public CommandResponse execute(CommandRequest cr){
		//Generate the payload
		JSONObject payload = new JSONObject();
		payload.put("serverTime", System.currentTimeMillis());
		
		return new CommandResponse(getName(), payload);
	}

}
