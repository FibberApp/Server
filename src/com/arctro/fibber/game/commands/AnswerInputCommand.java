package com.arctro.fibber.game.commands;

import java.util.List;

import org.json.JSONException;

import com.arctro.fibber.controllers.ResourceManager;
import com.arctro.fibber.game.Game;
import com.arctro.fibber.game.GameState;
import com.arctro.fibber.models.Answer;
import com.arctro.fibber.models.CommandRequest;
import com.arctro.fibber.models.CommandResponse;
import com.arctro.fibber.models.Player;
import com.arctro.fibber.supporting.exceptions.InvalidGameStateException;

/**
 * Receives and processes the command to input a command
 * @author Ben McLean
 */
public class AnswerInputCommand implements Command{

	Game game;

	public AnswerInputCommand(Game game){
		this.game = game;
	}

	@Override
	public String getName(){
		return Command.INPUT_ANSWER;
	}

	@Override
	public CommandResponse execute(CommandRequest cr){
		try{
			execute(cr.getSessionId(), cr.getPayload().getString("answer"));
		}catch(JSONException | InvalidGameStateException e){
			e.printStackTrace();
			return CommandResponse.exception(e);
		}
		
		return null;
	}

	public void execute(String sessionId, String answerStr) throws InvalidGameStateException{
		//Check if the game is in the correct state
		if(game.getState() != GameState.SUBMIT_ANSWER){
			throw new InvalidGameStateException("The game state must be in SUBMIT_ANSWER");
		}
		
		//Get the player that submitted the answer
		Player player = game.getPlayer(sessionId);
		
		//Create an answer object to hold the information
		Answer answer = new Answer(null, player, answerStr, false, (answerStr == null));
		answer.setAnswerId(ResourceManager.lease(answer));

		//Check if the player has previously submitted an answer (sanity check)
		List<Answer> answers = game.getPlayerAnswers();
		for(Answer a: answers){
			if(answer.getAnswer().equals(player)){
				answers.remove(a);
				break;
			}
		}
		
		//Add the answer
		answers.add(answer);
		
		//Check if all players have submitted answers, if they have interrupt
		if(answers.size() == game.getPlayers().size()){
			game.interrupt();
		}
	}

}
