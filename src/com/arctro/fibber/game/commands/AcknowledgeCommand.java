package com.arctro.fibber.game.commands;

import com.arctro.fibber.game.Game;
import com.arctro.fibber.models.CommandRequest;
import com.arctro.fibber.models.CommandResponse;

public class AcknowledgeCommand implements Command{
	
	Game game;
	
	public AcknowledgeCommand(Game game){
		this.game = game;
	}

	@Override
	public String getName(){
		return Command.ACKNOWLEDGE;
	}

	@Override
	public CommandResponse execute(CommandRequest cr){
		//Get the game builder the player is in
		game.setNumAcknowledged(game.getNumAcknowledged()+1);
		
		if(game.getNumAcknowledged() >= game.getPlayers().size()){
			game.interrupt();
		}
		
		return null;
	}
}
