package com.arctro.fibber.game.commands;

import org.json.JSONObject;

import com.arctro.fibber.game.controllers.GameBuilder;
import com.arctro.fibber.game.controllers.GameBuilderController;
import com.arctro.fibber.models.CommandRequest;
import com.arctro.fibber.models.CommandResponse;
import com.arctro.fibber.models.Player;

/**
 * Creates a game builder and returns the "gameCode" and player information
 * @author Ben McLean
 */
public class CreateGameBuilderCommand implements Command{

	public CreateGameBuilderCommand(){}

	@Override
	public String getName(){
		return Command.CREATE_GAME;
	}

	@Override
	public CommandResponse execute(CommandRequest cr){
		//Create a GameBuilder
		GameBuilder gb = GameBuilderController.create();
		
		//Generate the payload
		JSONObject payload = new JSONObject();
		payload.put("gameCode", gb.getGameCode());
		
		Player player = gb.join(cr.getSession(), cr.getPayload().getString("name"));
		payload.put("player", new JSONObject(player));
		
		return new CommandResponse("", payload);
	}

}
