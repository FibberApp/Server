package com.arctro.fibber.game.commands;

import com.arctro.fibber.game.Game;
import com.arctro.fibber.game.controllers.GameBuilder;
import com.arctro.fibber.game.controllers.GameBuilderController;
import com.arctro.fibber.game.controllers.GameController;
import com.arctro.fibber.models.CommandRequest;
import com.arctro.fibber.models.CommandResponse;

/**
 * Builds the game
 * @author Ben McLean
 */
public class BuildGameCommand implements Command{

	public BuildGameCommand(){}

	@Override
	public String getName(){
		return Command.BUILD_GAME;
	}

	@Override
	public CommandResponse execute(CommandRequest cr){
		//Get the game builder the player is in
		GameBuilder gb = GameBuilderController.getByPlayer(cr.getSessionId());
		
		//Check if the game builder exists (sanity check)
		if(gb == null){
			return null;
		}
		
		//Build the game
		Game g = gb.build();
		//Register the game
		GameController.register(g);
		
		//Start the game
		g.start();
		
		return null;
	}

}
