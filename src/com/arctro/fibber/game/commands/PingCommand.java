package com.arctro.fibber.game.commands;

import org.json.JSONObject;

import com.arctro.fibber.models.CommandRequest;
import com.arctro.fibber.models.CommandResponse;
import com.arctro.fibber.supporting.Consts;

/**
 * Ping
 * @author Ben McLean
 */
public class PingCommand implements Command{

	public PingCommand(){}

	@Override
	public String getName(){
		return Command.PING;
	}

	@Override
	public CommandResponse execute(CommandRequest cr){
		JSONObject payload = cr.getPayload();
		payload.put("sessionId", cr.getSession().getUserProperties().get(Consts.SESSION_ID_PARAM));
		
		return new CommandResponse("pong", payload);
	}

}
