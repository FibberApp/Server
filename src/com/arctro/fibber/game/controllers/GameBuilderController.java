package com.arctro.fibber.game.controllers;

import java.util.HashMap;
import java.util.Iterator;

import com.arctro.fibber.models.Player;
import com.arctro.fibber.supporting.Utils;

/**
 * Manages instances of GameBuilders, and which players are within them
 * @author Ben McLean
 *
 */
public class GameBuilderController{
	/**
	 * Stores game builders<br/>
	 * P1: Game code<br/>
	 * P2: The associated game builder
	 */
	static HashMap<String, GameBuilder> gameBuilders = new HashMap<String, GameBuilder>();
	
	/**
	 * Stores currently used game codes<br/>
	 * P1: Game Code<br/>
	 * P2: Not used
	 */
	static HashMap<String, Object> gameCodesInUse = new HashMap<String, Object>();
	
	/**
	 * Stores players and their associated game builder<br/>
	 * P1: Player Resource ID<br/>
	 * P2: The associated game builder
	 */
	static HashMap<String, GameBuilder> playersInGameBuilders = new HashMap<String, GameBuilder>();
	
	/**
	 * Creates a new game builder
	 * @return A new game builder
	 */
	public static GameBuilder create(){
		//Generate a game code
		String gameCode = getGameCode();
		//Create a new game builder
		GameBuilder gb = new GameBuilder(gameCode);
		
		//Register game builder
		gameBuilders.put(gameCode, gb);
		
		//Return the game builder
		return gb;
	}
	
	/**
	 * Gets the game builder from it's game code
	 * @param gameCode The game builder to get
	 * @return The associated game builder
	 */
	public static GameBuilder get(String gameCode){
		return gameBuilders.get(gameCode);
	}
	
	/**
	 * Gets the player's game builder
	 * @param session The player to get the game builder for
	 * @return The associated game builder
	 */
	public static GameBuilder getByPlayer(String session){
		return playersInGameBuilders.get(session);
	}
	
	/**
	 * Removes a game builder
	 * @param gc Game code of the builder to remove
	 */
	static void removeGameBuilder(String gc){
		GameBuilder gb = gameBuilders.remove(gc);
		
		//Remove all the game builder's players
		Iterator<Player> players = gb.getCurrentPlayers().iterator();
		while(players.hasNext()){
			Player player = players.next();
			playersInGameBuilders.remove(player.getSessionId());
		}
		
		//Remove game code
		gameCodesInUse.remove(gc);
	}
	
	/**
	 * Generate a unique game code
	 * @return A unique game code
	 */
	private static String getGameCode(){
		String gameCode;
		//Loop until a unique game code is found
		do{
			gameCode = Utils.randomOfLength(5) + "";
		}while(gameCodesInUse.containsKey(gameCode));
		
		gameCodesInUse.put(gameCode, "");
		
		return gameCode;
	}
}
