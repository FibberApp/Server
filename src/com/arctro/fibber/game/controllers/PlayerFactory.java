package com.arctro.fibber.game.controllers;

import com.arctro.fibber.models.Player;

public class PlayerFactory{
	
	public static String[] icons = new String[]{"https://i.imgur.com/3nuwWCB.jpg"};
	int index = 0;
	
	public PlayerFactory(){}
	
	public Player create(String name, String sessionId){
		return new Player(
				sessionId,
				name,
				icons[index=(index+1)%icons.length]
				);
	}
	
}
