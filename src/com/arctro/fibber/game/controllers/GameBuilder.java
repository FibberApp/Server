package com.arctro.fibber.game.controllers;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;

import javax.websocket.Session;

import com.arctro.fibber.broadcasts.PlayerJoinBroadcast;
import com.arctro.fibber.broadcasts.PlayerLeaveBroadcast;
import com.arctro.fibber.controllers.CategoryController;
import com.arctro.fibber.controllers.ResourceManager;
import com.arctro.fibber.game.Game;
import com.arctro.fibber.game.commands.AcknowledgeCommand;
import com.arctro.fibber.game.commands.AnswerInputCommand;
import com.arctro.fibber.game.commands.CategorySelectCommand;
import com.arctro.fibber.game.commands.Command;
import com.arctro.fibber.game.commands.SelectAnswerCommand;
import com.arctro.fibber.models.Player;
import com.arctro.fibber.supporting.Consts;

/**
 * Produces games. Manages player joining, getting the game's categories
 * assigning commands to the game instance, and multiple other tasks
 * @author Ben McLean
 *
 */
public class GameBuilder{
	Game game;
	String gameCode;
	PlayerFactory playerFactory;
	
	/**
	 * Create a new game instance with the specified game code
	 * @param gameCode The code that allows other players to join
	 */
	public GameBuilder(String gameCode){
		this.gameCode = gameCode;
		
		game = new Game();
		//Get a new resource ID for the game
		game.setGameId(ResourceManager.lease(game));
		
		//Create a new player factory
		playerFactory = new PlayerFactory();
		
		try{
			//Setup the default category pool
			game.setCategoryPool(CategoryController.getDefaultCategories());
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Adds a player to the game
	 * @param session The session of the joining player
	 * @param name The name of the joining player
	 * @return The player object
	 */
	public Player join(Session session, String name){
		//Gets the player's resource ID
		String sid = (String) session.getUserProperties().get(Consts.SESSION_ID_PARAM);
		//Generates a new player
		Player player = playerFactory.create(name, sid);
		
		//Broadcasts to currently joined players that a new player has joined
		new PlayerJoinBroadcast()
			.setJoined(player)
			.addRecipients(
					game.getPlayers().values()
					)
			.broadcast();
		
		//Adds the player to the game
		game.getPlayers().put(sid, player);
		
		//Registers the player
		GameBuilderController.playersInGameBuilders.put(sid, this);
		
		return player;
	}
	
	/**
	 * Removes a player from a game
	 * @param session The resource ID of the player to leave
	 */
	public void leave(String session){
		//Removes the player from the game
		Player player = game.getPlayers().remove(session);
		
		//Broadcasts to all joined players that the player has left
		new PlayerLeaveBroadcast()
			.setLeft(player)
			.addRecipients(game.getPlayers().values())
			.broadcast();
		
		//Deregisters the player
		GameBuilderController.playersInGameBuilders.remove(session);
	}
	
	/**
	 * Gets the current players
	 * @return The current players
	 */
	public Collection<Player> getCurrentPlayers(){
		return game.getPlayers().values();
	}
	
	/**
	 * Builds and returns a complete game object
	 * @return The game
	 */
	public Game build(){
		//Deregister game builder
		GameBuilderController.removeGameBuilder(gameCode);
		
		//Register game commands
		HashMap<String, Command> gameCommands = game.getCommands();
		
		//Add the Category Select Command
		CategorySelectCommand csc = new CategorySelectCommand(game);
		gameCommands.put(csc.getName(), csc);
		
		//Add the Answer Input Command
		AnswerInputCommand aic = new AnswerInputCommand(game);
		gameCommands.put(aic.getName(), aic);
		
		//Add the Select Answer Command
		SelectAnswerCommand sac = new SelectAnswerCommand(game);
		gameCommands.put(sac.getName(), sac);
		
		AcknowledgeCommand ac = new AcknowledgeCommand(game);
		gameCommands.put(ac.getName(), ac);
		
		return game;
	}

	public String getGameCode(){
		return gameCode;
	}

	public void setGameCode(String gameCode){
		this.gameCode = gameCode;
	}
}
