package com.arctro.fibber.game.controllers;

import java.util.Collection;
import java.util.HashMap;

import com.arctro.fibber.game.Game;
import com.arctro.fibber.models.Player;

public class GameController{
	static HashMap<String, Game> activeGames = new HashMap<String, Game>(); //<GameUUID, Game>
	static HashMap<String, Game> gamePlayers = new HashMap<String, Game>(); //<SessionID, Game>
	
	public static void register(Game game){
		activeGames.put(game.getGameId(), game);
		
		for(Player player: game.getPlayers().values()){
			gamePlayers.put(player.getSessionId(), game);
		}
	}
	
	public static Game deregister(String gameId){
		Game game = activeGames.remove(gameId);
		
		if(game == null){
			return game;
		}
		
		Collection<Player> players = game.getPlayers().values();
		
		for(Player player: players){
			gamePlayers.remove(player.getSessionId());
		}
		
		return game;
	}
	
	public static Game getGameByPlayer(String sessionId){
		return gamePlayers.get(sessionId);
	}
}
