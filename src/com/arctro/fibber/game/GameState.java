package com.arctro.fibber.game;

public enum GameState{
	PLAYER_JOIN,
	CATEGORY_SELECT,
	SUBMIT_ANSWER,
	SELECT_ANSWER,
	NO_INTERACT,
	ACKNOWLEDGE
}
