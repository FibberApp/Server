package com.arctro.fibber.models;

import javax.websocket.Session;

import org.json.JSONObject;

import com.arctro.fibber.supporting.Consts;

public class CommandRequest{
	String command;
	Session session;
	String requestToken;
	JSONObject payload;

	public CommandRequest(String command, Session session, String requestToken, JSONObject payload){
		super();
		this.command = command;
		this.session = session;
		this.requestToken = requestToken;
		this.payload = payload;
	}

	public String getCommand(){
		return command;
	}

	public void setCommand(String command){
		this.command = command;
	}

	public Session getSession(){
		return session;
	}

	public void setSession(Session session){
		this.session = session;
	}
	
	public String getSessionId(){
		return (String) session.getUserProperties().get(Consts.SESSION_ID_PARAM);
	}
	
	public String getRequestToken(){
		return requestToken;
	}

	public void setRequestToken(String requestToken){
		this.requestToken = requestToken;
	}

	public JSONObject getPayload(){
		return payload;
	}

	public void setPayload(JSONObject payload){
		this.payload = payload;
	}
}
