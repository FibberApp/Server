package com.arctro.fibber.models;

public class Question{
	int questionId;
	Category category;
	String question;
	
	public Question(int questionId, Category category, String question){
		super();
		this.questionId = questionId;
		this.category = category;
		this.question = question;
	}

	public int getQuestionId(){
		return questionId;
	}

	public void setQuestionId(int questionId){
		this.questionId = questionId;
	}

	public Category getCategory(){
		return category;
	}

	public void setCategory(Category category){
		this.category = category;
	}

	public String getQuestion(){
		return question;
	}

	public void setQuestion(String question){
		this.question = question;
	}
}
