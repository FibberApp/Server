package com.arctro.fibber.models;

import org.json.JSONObject;

public class JSONAnswerSession{
	JSONObject answer;
	String session;
	
	public JSONAnswerSession(JSONObject answer, String session){
		super();
		this.answer = answer;
		this.session = session;
	}

	public JSONObject getAnswer(){
		return answer;
	}

	public void setAnswer(JSONObject answer){
		this.answer = answer;
	}

	public String getSession(){
		return session;
	}

	public void setSession(String session){
		this.session = session;
	}
}
