package com.arctro.fibber.models;

import java.util.TimeZone;

/**
 * Holds information pertaining to time limits
 * @author Ben McLean
 */
public class TimeLimit{
	/**
	 * The absolute value of the time limit
	 */
	long absolute;
	/**
	 * The offset that absolute value is from UTC
	 */
	long UTCOffset;
	
	/**
	 * How much time is left (relatively)
	 */
	int left;
	
	public TimeLimit(long absolute, int left, long uTCOffset){
		super();
		this.absolute = absolute;
		this.left = left;
		UTCOffset = uTCOffset;
	}

	public TimeLimit(long absolute, int left){
		super();
		this.absolute = absolute;
		this.left = left;
		
		TimeZone tz = TimeZone.getDefault();
		UTCOffset = tz.getRawOffset();
	}
	
	public TimeLimit(int left){
		super();
		this.absolute = System.currentTimeMillis() + (long)(left);
		this.left = left;
		
		TimeZone tz = TimeZone.getDefault();
		UTCOffset = tz.getRawOffset();
	}

	public long getAbsolute(){
		return absolute;
	}

	public void setAbsolute(long absolute){
		this.absolute = absolute;
	}

	public int getLeft(){
		return left;
	}

	public void setLeft(int left){
		this.left = left;
	}

	public long getUTCOffset(){
		return UTCOffset;
	}

	public void setUTCOffset(long uTCOffset){
		UTCOffset = uTCOffset;
	}
}
