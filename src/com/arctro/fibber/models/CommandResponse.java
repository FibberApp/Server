package com.arctro.fibber.models;

import org.json.JSONObject;

import com.arctro.fibber.broadcasts.Broadcast;

public class CommandResponse{
	String command;
	String requestToken;
	JSONObject payload;
	
	public CommandResponse(String command, JSONObject payload){
		super();
		this.command = command;
		this.payload = payload;
	}

	public CommandResponse(String command, String requestToken, JSONObject payload){
		super();
		this.command = command;
		this.requestToken = requestToken;
		this.payload = payload;
	}

	public String getCommand(){
		return command;
	}

	public String getRequestToken(){
		return requestToken;
	}

	public void setRequestToken(String requestToken){
		this.requestToken = requestToken;
	}

	public void setCommand(String command){
		this.command = command;
	}

	public JSONObject getPayload(){
		return payload;
	}

	public void setPayload(JSONObject payload){
		this.payload = payload;
	}
	
	public static CommandResponse exception(String requestToken, Exception e){
		JSONObject j = new JSONObject();
		j.put("name", e.getClass().getName());
		j.put("message", e.getMessage());
		return new CommandResponse(Broadcast.EXCEPTION, requestToken, j);
	}
	
	public static CommandResponse exception(Exception e){
		JSONObject j = new JSONObject();
		j.put("name", e.getClass().getName());
		j.put("message", e.getMessage());
		return new CommandResponse(Broadcast.EXCEPTION, j);
	}
	
	public static CommandResponse blank(){
		return new CommandResponse("",new JSONObject());
	}
}
