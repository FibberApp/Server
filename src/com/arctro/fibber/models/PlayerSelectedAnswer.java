package com.arctro.fibber.models;

import java.util.List;

/**
 * Stores who selected an answer
 * @author Ben McLean
 *
 */
public class PlayerSelectedAnswer{
	Answer answer;
	List<Player> players;
	
	public PlayerSelectedAnswer(Answer answer, List<Player> players){
		super();
		this.answer = answer;
		this.players = players;
	}

	public Answer getAnswer(){
		return answer;
	}

	public void setAnswer(Answer answer){
		this.answer = answer;
	}

	public List<Player> getPlayers(){
		return players;
	}

	public void setPlayers(List<Player> players){
		this.players = players;
	}
	
	
}
