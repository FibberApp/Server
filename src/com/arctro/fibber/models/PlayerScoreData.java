package com.arctro.fibber.models;

public class PlayerScoreData{
	Player player;
	int scored;
	
	public PlayerScoreData(Player player, int scored){
		super();
		this.player = player;
		this.scored = scored;
	}

	public Player getPlayer(){
		return player;
	}

	public void setPlayer(Player player){
		this.player = player;
	}

	public int getScored(){
		return scored;
	}

	public void setScored(int scored){
		this.scored = scored;
	}
}
