package com.arctro.fibber.models;

/**
 * Holds an answer
 * @author Ben McLean
 *
 */
public class Answer{
	String answerId;
	Player answerer;
	String answer;
	boolean computerAnswer;
	boolean forfit;

	public Answer(String answerId, Player answerer, String answer, boolean computerAnswer, boolean forfit){
		super();
		this.answerId = answerId;
		this.answerer = answerer;
		this.answer = answer;
		this.computerAnswer = computerAnswer;
		this.forfit = forfit;
	}

	public String getAnswerId(){
		return answerId;
	}

	public void setAnswerId(String answerId){
		this.answerId = answerId;
	}

	public String getAnswer(){
		return answer;
	}

	public void setAnswer(String answer){
		this.answer = answer;
	}

	public Player getAnswerer(){
		return answerer;
	}
	
	public void setAnswerer(Player answerer){
		this.answerer = answerer;
	}
	
	public boolean isComputerAnswer(){
		return computerAnswer;
	}
	
	public void setComputerAnswer(boolean computerAnswer){
		this.computerAnswer = computerAnswer;
	}
	
	public boolean isForfit(){
		return forfit;
	}
	
	public void setForfit(boolean forfit){
		this.forfit = forfit;
	}
}
