package com.arctro.fibber.models;

public class Player{
	
	String sessionId;
	String name;
	String icon;
	int score;

	public Player(String sessionId, String name, String icon){
		super();
		this.sessionId = sessionId;
		this.name = name;
		this.icon = icon;
		score = 0;
	}

	public String getSessionId(){
		return sessionId;
	}

	public void setSessionId(String sessionId){
		this.sessionId = sessionId;
	}

	public String getName(){
		return name;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getIcon(){
		return icon;
	}

	public void setIcon(String icon){
		this.icon = icon;
	}

	public int getScore(){
		return score;
	}

	public void setScore(int score){
		this.score = score;
	}

}
