package com.arctro.fibber.broadcasts;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.arctro.fibber.controllers.ResourceManager;
import com.arctro.fibber.models.Answer;
import com.arctro.fibber.models.CommandResponse;
import com.arctro.fibber.models.JSONAnswerSession;
import com.arctro.fibber.models.Player;
import com.arctro.fibber.models.TimeLimit;
import com.arctro.fibber.websocket.WebsocketWrapper;

public class SelectAnswerBroadcast extends Broadcast{
	
	List<Answer> answers;
	TimeLimit timeLimit;

	public SelectAnswerBroadcast setAnswers(List<Answer> answers){
		this.answers = answers;
		return this;
	}
	
	public SelectAnswerBroadcast setTimeLimit(TimeLimit timeLimit){
		this.timeLimit = timeLimit;
		return this;
	}

	@Override
	protected CommandResponse build(){
		JSONObject payload = new JSONObject();
		
		JSONArray answersJ = new JSONArray();
		for(Answer answer: answers){
			JSONObject answerJ = new JSONObject();
			answerJ.put("answerId", answer.getAnswerId());
			answerJ.put("answer", answer.getAnswer());
			
			answersJ.put(answerJ);
		}
		
		payload.put("answers", answersJ);
		payload.put("timeLimit", new JSONObject(timeLimit));
		return new CommandResponse(getName(), payload);
	}
	
	@Override
	public void broadcast(){
		JSONObject payload = new JSONObject();
		payload.put("timeLimit", new JSONObject(timeLimit));
		
		List<JSONAnswerSession> answersJ = new ArrayList<JSONAnswerSession>();
		for(Answer answer: answers){
			JSONObject answerJ = new JSONObject();
			answerJ.put("answerId", answer.getAnswerId());
			answerJ.put("answer", answer.getAnswer());
			
			Player answerer = answer.getAnswerer();
			
			answersJ.add(new JSONAnswerSession(answerJ, (answerer == null) ? "" : answerer.getSessionId()));
		}
		
		//Broadcast to the sessions
		for(String session: sessions){
			JSONArray answersJS = new JSONArray();
			for(JSONAnswerSession answer: answersJ){
				if(!session.equals(answer.getSession())){
					answersJS.put(answer.getAnswer());
				}
			}
			
			payload.put("answers", answersJS);
			
			CommandResponse cr = new CommandResponse(getName(), payload);
			
			//Get the session from ResourceManager
			try{
				//Get the remote and broadcast
				WebsocketWrapper ww = (WebsocketWrapper) ResourceManager.getById(session);
				if(ww != null){
					ww.transmit(new JSONObject(cr).toString());
				}
			}catch(IOException e){
				e.printStackTrace();
			}
		}
	}

	@Override
	protected String getName(){
		return Broadcast.SELECT_ANSWER;
	}

}
