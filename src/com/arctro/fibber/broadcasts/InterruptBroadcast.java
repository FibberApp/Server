package com.arctro.fibber.broadcasts;

import org.json.JSONObject;

import com.arctro.fibber.models.CommandResponse;

/**
 * Handles broadcasting Interrupt broadcasts
 * @author Ben McLean
 *
 */
public class InterruptBroadcast extends Broadcast{
	
	@Override
	protected CommandResponse build(){
		return new CommandResponse(getName(), new JSONObject());
	}

	@Override
	protected String getName(){
		return Broadcast.INTERRUPT;
	}

}
