package com.arctro.fibber.broadcasts;

import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONObject;

import com.arctro.fibber.models.CommandResponse;
import com.arctro.fibber.models.PlayerScoreData;
import com.arctro.fibber.models.PlayerSelectedAnswer;

/**
 * Broadcasts which answers the players selected and their new scores
 * @author Ben McLean
 *
 */
public class PlayerSelectedAnswerBroadcast extends Broadcast{
	
	Collection<PlayerScoreData> psd;
	Collection<PlayerSelectedAnswer> psa;
	
	public PlayerSelectedAnswerBroadcast setPlayerScoreData(Collection<PlayerScoreData> psd){
		this.psd = psd;
		return this;
	}
	
	public PlayerSelectedAnswerBroadcast setPlayerSelectedAnswers(Collection<PlayerSelectedAnswer> psa){
		this.psa = psa;
		return this;
	}

	@Override
	protected CommandResponse build(){
		JSONObject payload = new JSONObject();
		payload.put("playerScores", new JSONArray(psd));
		payload.put("playerSelectedAnswers", new JSONArray(psa));
		
		return new CommandResponse(getName(), payload);
	}

	@Override
	protected String getName(){
		return Broadcast.PLAYER_SELECTED_ANSWER;
	}

}
