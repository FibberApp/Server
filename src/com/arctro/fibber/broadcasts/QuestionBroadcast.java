package com.arctro.fibber.broadcasts;

import org.json.JSONObject;

import com.arctro.fibber.models.CommandResponse;
import com.arctro.fibber.models.Question;
import com.arctro.fibber.models.TimeLimit;

public class QuestionBroadcast extends Broadcast{
	
	Question question;
	TimeLimit timeLimit;

	public QuestionBroadcast setQuestion(Question question){
		this.question = question;
		return this;
	}
	
	public QuestionBroadcast setTimeLimit(TimeLimit timeLimit){
		this.timeLimit = timeLimit;
		return this;
	}

	@Override
	protected CommandResponse build(){
		JSONObject payload = new JSONObject();
		payload.put("question", new JSONObject(question));
		payload.put("timeLimit", new JSONObject(timeLimit));
		return new CommandResponse(getName(), payload);
	}

	@Override
	protected String getName(){
		return Broadcast.QUESTION;
	}

}
