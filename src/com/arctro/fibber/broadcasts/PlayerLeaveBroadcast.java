package com.arctro.fibber.broadcasts;

import org.json.JSONObject;

import com.arctro.fibber.models.CommandResponse;
import com.arctro.fibber.models.Player;

public class PlayerLeaveBroadcast extends Broadcast{
	
	Player left;

	public Player getLeft(){
		return left;
	}

	public PlayerLeaveBroadcast setLeft(Player left){
		this.left = left;
		return this;
	}

	@Override
	protected CommandResponse build(){
		JSONObject payload = new JSONObject();
		payload.put("left", new JSONObject(left));
		return new CommandResponse(getName(), payload);
	}

	@Override
	protected String getName(){
		return Broadcast.PLAYER_LEAVE;
	}

}
