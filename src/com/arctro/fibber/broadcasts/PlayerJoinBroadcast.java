package com.arctro.fibber.broadcasts;

import org.json.JSONObject;

import com.arctro.fibber.models.CommandResponse;
import com.arctro.fibber.models.Player;

public class PlayerJoinBroadcast extends Broadcast{
	
	Player joined;

	public Player getJoined(){
		return joined;
	}

	public PlayerJoinBroadcast setJoined(Player joined){
		this.joined = joined;
		return this;
	}

	@Override
	protected CommandResponse build(){
		JSONObject payload = new JSONObject();
		payload.put("joined", new JSONObject(joined));
		return new CommandResponse(getName(), payload);
	}

	@Override
	protected String getName(){
		return Broadcast.PLAYER_JOIN;
	}

}
