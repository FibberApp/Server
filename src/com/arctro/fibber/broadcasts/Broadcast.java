package com.arctro.fibber.broadcasts;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.json.JSONObject;

import com.arctro.fibber.controllers.ResourceManager;
import com.arctro.fibber.models.CommandResponse;
import com.arctro.fibber.models.Player;
import com.arctro.fibber.supporting.JSMap;
import com.arctro.fibber.websocket.WebsocketWrapper;

/**
 * A baseplate for all "broadcasts". Broadcasts are messages sent
 * to clients
 * @author Ben McLean
 *
 */
public abstract class Broadcast{
	
	/**
	 * Interrupt broadcast name
	 */
	public static final String INTERRUPT = "interrupt";
	/**
	 * Category Select broadcast name
	 */
	public static final String CATEGORY_SELECT_POOL = "category_select";
	/**
	 * Question broadcast name
	 */
	public static final String QUESTION = "question";
	/**
	 * Select Answer broadcast name
	 */
	public static final String SELECT_ANSWER = "select_answer";
	/**
	 * Player Join broadcast name
	 */
	public static final String PLAYER_JOIN = "player_join";
	/**
	 * Player Leave broadcast name
	 */
	public static final String PLAYER_LEAVE = "player_leave";
	/**
	 * Player Selected Answer broadcast name
	 */
	public static final String PLAYER_SELECTED_ANSWER = "player_selected_answer";
	/**
	 * Exception broadcast name
	 */
	public static final String EXCEPTION = "exception";
	
	//A list of all sessions to broadcast to
	List<String> sessions;
	
	public Broadcast(){
		//Init sessions
		sessions = new ArrayList<String>();
	}
	
	/**
	 * Adds new recipients by session ID
	 * @param sessions A list of session ids
	 * @return Broadcast
	 */
	public Broadcast addRecipientsBySession(Collection<String> sessions){
		this.sessions.addAll(sessions);	
		return this;
	}
	
	/**
	 * Adds new recipients by player
	 * @param player A list of players
	 * @return Broadcast
	 */
	public Broadcast addRecipients(Collection<Player> players){
		addRecipientsBySession(JSMap.playerToSession(players));	
		return this;
	}
	
	/**
	 * Adds a new recipient by session ID
	 * @param session A session id
	 * @return Broadcast
	 */
	public Broadcast addRecipient(String session){
		sessions.add(session);	
		return this;
	}
	
	/**
	 * Removes a recipient by session ID
	 * @param session A session ID
	 * @return Broadcast
	 */
	public Broadcast removeRecipient(String session){
		sessions.remove(session);	
		return this;
	}
	
	/**
	 * Removes a list of recipients by session ID
	 * @param sessions A list of session ids
	 * @return Broadcast
	 */
	public Broadcast removeRecipient(List<String> sessions){
		this.sessions.removeAll(sessions);	
		return this;
	}
	
	/**
	 * Builds the command response. This function is overwritten by children
	 * @return A CommandResponse ready to broadcast
	 */
	protected abstract CommandResponse build();
	
	/**
	 * Returns the command's name
	 * @return The command's name
	 */
	protected abstract String getName();
	
	/**
	 * Broadcasts the built CommandResponse
	 */
	public void broadcast(){
		//Get the payload from build()
		JSONObject payload = new JSONObject(build());
		
		//Broadcast to the sessions
		for(String session: sessions){
			//Get the session from ResourceManager
			try{
				WebsocketWrapper ww = (WebsocketWrapper) ResourceManager.getById(session);
				if(ww != null){
					ww.transmit(payload.toString());
				}
			}catch(IOException e){
				e.printStackTrace();
			}
		}
	}
	
}
