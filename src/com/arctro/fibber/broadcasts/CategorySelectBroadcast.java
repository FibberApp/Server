package com.arctro.fibber.broadcasts;

import java.io.IOException;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.arctro.fibber.controllers.ResourceManager;
import com.arctro.fibber.models.Category;
import com.arctro.fibber.models.CommandResponse;
import com.arctro.fibber.models.Player;
import com.arctro.fibber.models.TimeLimit;
import com.arctro.fibber.websocket.WebsocketWrapper;

/**
 * Handles sending Category Select broadcasts.
 * @author Ben McLean
 *
 */
public class CategorySelectBroadcast extends Broadcast{

	Player selector;
	List<Category> categories;
	TimeLimit timeLimit;
	
	public CategorySelectBroadcast setSelector(Player selector){
		this.selector = selector;
		return this;
	}

	public CategorySelectBroadcast setCategories(List<Category> categories){
		this.categories = categories;
		return this;
	}
	
	public CategorySelectBroadcast setTimeLimit(TimeLimit timeLimit){
		this.timeLimit = timeLimit;
		return this;
	}

	@Override
	protected CommandResponse build(){
		JSONObject payload = new JSONObject();
		payload.put("categories", new JSONArray(categories));
		payload.put("selector", new JSONObject(selector));
		payload.put("timeLimit", new JSONObject(timeLimit));
		return new CommandResponse(getName(), payload);
	}

	@Override
	protected String getName(){
		return Broadcast.CATEGORY_SELECT_POOL;
	}
	
	@Override
	public void broadcast(){
		for(String session: sessions){
			JSONObject payload = new JSONObject(build());
			//If the user is the picker modify their payload so it tells them they are
			if(session.equals(selector.getSessionId())){
				JSONObject payloadPayload = (JSONObject) payload.get("payload");
				payloadPayload.put("pick", true);
				payload.put("payload", payloadPayload);
			}
			
			try{
				WebsocketWrapper ww = (WebsocketWrapper) ResourceManager.getById(session);
				if(ww != null){
					ww.transmit(payload.toString());
				}
			}catch(IOException e){
				e.printStackTrace();
			}
		}
	}

}
